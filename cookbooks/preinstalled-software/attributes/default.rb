default[:visual_studio_build_tool] = {
  'version': '16.2.3.0',
  'desktop_workload_version': '1.0.1',
  'dotnetcore_workload_version': '1.0.0',
  'cpp_workload_version': '1.0.0',
  'web_workload_version': '1.0.0',
  'mobile_workload_version': '1.0.0',
}

default[:utils] = {
  'zip_version': '19.0',
  'wget_version': '1.20.3.20190531',
  'curl_version': '7.65.3',
  'jq_version': '1.5',
  'docker_compose_version': '1.24.0',
  'nuget_version': '5.2.0',
}

default[:languages] = {
  'dotnetcore_version': '2.2.401',
  'ruby_version': '2.6.3.1',
  'go_version': '1.13',
  'nodejs_version': '12.10.0',
  'openjdk_version': '12.0.2',
}
